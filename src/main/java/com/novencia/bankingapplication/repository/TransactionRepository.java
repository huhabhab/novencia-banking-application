package com.novencia.bankingapplication.repository;

import com.novencia.bankingapplication.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>
{
	List<Transaction> findByAccountIdFkAndTransactionTypeFk(long accountIdFk, int transactionTypeFk);

	List<Transaction> findByAccountIdFk(long accountIdFk);

}


