package com.novencia.bankingapplication.repository;

import com.novencia.bankingapplication.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BalanceRepository extends JpaRepository<Account, Long>
{

	@Query(value = "select acc.id as accountId, acc.balance*er.rate as balance from banking.account acc join banking.exchange_rate er on er.currency_id_fk  = acc.currency_id_fk where acc.customer_id_fk = ?1", nativeQuery = true)
	List<Object> findBalances(int customerId);
}



