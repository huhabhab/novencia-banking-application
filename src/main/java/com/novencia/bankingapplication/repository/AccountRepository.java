package com.novencia.bankingapplication.repository;

import com.novencia.bankingapplication.entity.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long>
{
}



