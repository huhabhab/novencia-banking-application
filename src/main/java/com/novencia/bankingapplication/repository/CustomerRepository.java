package com.novencia.bankingapplication.repository;

import com.novencia.bankingapplication.entity.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long>
{

}



