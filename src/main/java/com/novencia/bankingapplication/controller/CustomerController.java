package com.novencia.bankingapplication.controller;

import com.novencia.bankingapplication.entity.Customer;
import com.novencia.bankingapplication.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/customer")

public class CustomerController
{
	@Autowired
	CustomerService customerService;

	@GetMapping("/all")
	public ResponseEntity<List<Customer>> getAllCustomers()
	{
		List<Customer> customers = customerService.getCustomers();
		return new ResponseEntity<>(customers, HttpStatus.OK);
	}

	@GetMapping({ "/{customerId}" })
	public ResponseEntity<Customer> getCustomer(@PathVariable Long customerId)
	{
		Customer c = customerService.getCustomerById(customerId);
		if(c != null)
		{
			return new ResponseEntity<>(c, HttpStatus.OK);

		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

	@PostMapping("/add")
	public ResponseEntity<Customer> addCustomer(@RequestBody @Valid Customer customer)
	{
		Customer localCustomer = customerService.insert(customer);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("CustomerId", localCustomer.getId().toString());
		return new ResponseEntity<>(localCustomer, httpHeaders, HttpStatus.CREATED);
	}

	@DeleteMapping({ "/delete/{customerId}" })
	public ResponseEntity<Customer> deleteCustomer(@PathVariable("customerId") Long customerId)
	{
		Long id = customerService.deleteCustomer(customerId);
		if(id != null)
		{
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
