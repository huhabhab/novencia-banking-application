package com.novencia.bankingapplication.controller;

import com.novencia.bankingapplication.entity.Account;
import com.novencia.bankingapplication.entity.Customer;
import com.novencia.bankingapplication.entity.Transaction;
import com.novencia.bankingapplication.repository.TransactionRepository;
import com.novencia.bankingapplication.service.AccountService;
import com.novencia.bankingapplication.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/transactions")

public class TransactionController
{

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	TransactionService transactionService;

	@Autowired
	AccountService accountService;

	@GetMapping("/deposite/{accountId}/")
	public ResponseEntity<List<Transaction>> getDepositeTransactions(@PathVariable("accountId") long accountId)
	{
		List<Transaction> transactions = transactionRepository.findByAccountIdFkAndTransactionTypeFk(accountId, 1);
		return new ResponseEntity<>(transactions, HttpStatus.OK);
	}

	@GetMapping("/withdrawal/{accountId}/")
	public ResponseEntity<List<Transaction>> getWithdrawTransactions(@PathVariable("accountId") long accountId)
	{
		List<Transaction> transactions = transactionRepository.findByAccountIdFkAndTransactionTypeFk(accountId, 2);
		return new ResponseEntity<>(transactions, HttpStatus.OK);
	}

	@GetMapping("/{accountId}/")
	public ResponseEntity<List<Transaction>> getAllAccountTransactions(@PathVariable("accountId") long accountId)
	{
		List<Transaction> transactions = transactionRepository.findByAccountIdFk(accountId);
		return new ResponseEntity<>(transactions, HttpStatus.OK);
	}

	@PostMapping("/deposite")
	public ResponseEntity<Transaction> deposit(@RequestBody @Valid Transaction transaction)
	{
		// Get Corresponding Account
		Account a = accountService.getAccountById(transaction.getAccountIdFk());

		transaction.setBalance(transaction.getAmount() + a.getBalance());
		transaction.setTransactionStatusFk(1);
		transaction.setTransactionTypeFk(1);

		Transaction localTransaction = transactionService.insert(transaction);

		// Update balance after success deposite
		a.setBalance(transaction.getAmount() + a.getBalance());
		accountService.updateAccount(a.getId(), a);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("TransactionId", localTransaction.getId().toString());
		return new ResponseEntity<>(localTransaction, httpHeaders, HttpStatus.CREATED);
	}

	@PostMapping("/withdraw")
	public ResponseEntity<Transaction> withdraw(@RequestBody @Valid Transaction transaction)
	{
		// Get Corresponding Account
		Account a = accountService.getAccountById(transaction.getAccountIdFk());

		transaction.setTransactionTypeFk(2);

		if(a.getBalance() >= transaction.getAmount())
		{
			transaction.setTransactionStatusFk(1);
			transaction.setBalance(a.getBalance() - transaction.getAmount());
			// Update balance after withdrawal
			a.setBalance(a.getBalance() - transaction.getAmount());
			accountService.updateAccount(a.getId(), a);
		}
		else // Not enough balance
		{
			transaction.setTransactionStatusFk(0);
			transaction.setBalance(a.getBalance());
		}
		Transaction localTransaction = transactionService.insert(transaction);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("TransactionId", localTransaction.getId().toString());
		return new ResponseEntity<>(localTransaction, httpHeaders, HttpStatus.CREATED);
	}

	@DeleteMapping({ "/delete/{transactionId}" })
	public ResponseEntity<Transaction> deleteTransaction(@PathVariable("transactionId") Long transactionId)
	{
		Long id = transactionService.deleteTransaction(transactionId);
		if(id != null)
		{
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
