package com.novencia.bankingapplication.controller;

import com.novencia.bankingapplication.entity.Account;
import com.novencia.bankingapplication.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/account")

public class AccountController
{
	@Autowired
	AccountService accountService;

	@GetMapping("/all")
	public ResponseEntity<List<Account>> getAllAccounts()
	{
		List<Account> accounts = accountService.getAccounts();
		return new ResponseEntity<>(accounts, HttpStatus.OK);
	}

	@GetMapping({ "/{accountId}" })
	public ResponseEntity<Account> getAccount(@PathVariable Long accountId)
	{
		Account a = accountService.getAccountById(accountId);
		if(a != null)
		{
			return new ResponseEntity<>(a, HttpStatus.OK);

		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

	@PostMapping("/add")
	public ResponseEntity<Account> addAccount(@RequestBody @Valid Account account)
	{

		Account localAccount = accountService.insert(account);
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("AccountId", localAccount.getId().toString());
		return new ResponseEntity<>(localAccount, httpHeaders, HttpStatus.CREATED);

	}

	@DeleteMapping({ "/delete/{accountId}" })
	public ResponseEntity<Account> deleteAccount(@PathVariable("accountId") Long accountId)
	{
		Long id = accountService.deleteAccount(accountId);
		if(id != null)
		{
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

}
