package com.novencia.bankingapplication.service;

import com.novencia.bankingapplication.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransactionService
{
	List<Transaction> getTransactions();

	Transaction getTransactionById(Long id);

	Transaction insert(Transaction transaction);

	void updateTransaction(Long id, Transaction transaction);

	Long deleteTransaction(Long transactionId);


}
