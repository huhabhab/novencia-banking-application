package com.novencia.bankingapplication.service;

import com.novencia.bankingapplication.entity.Customer;
import com.novencia.bankingapplication.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService
{
	CustomerRepository customerRepository;

	public CustomerServiceImpl(CustomerRepository customerRepository)
	{
		this.customerRepository = customerRepository;
	}

	@Override
	public List<Customer> getCustomers()
	{
		List<Customer> customers = new ArrayList<>();
		customerRepository.findAll().forEach(customers::add);
		return customers;
	}

	@Override
	public Customer getCustomerById(Long id)
	{
		return customerRepository.findById(id).get();
	}

	@Override
	public Customer insert(Customer customer)
	{
		return customerRepository.save(customer);
	}

	@Override
	public void updateCustomer(Long id, Customer customer)
	{
		Customer customerFromDb = customerRepository.findById(id).get();

		customerFromDb.setFirstName(customer.getFirstName());
		customerFromDb.setLastName(customer.getLastName());
		customerFromDb.setEmailAddress(customer.getEmailAddress());
		customerFromDb.setPhoneNumber(customer.getPhoneNumber());

		customerRepository.save(customerFromDb);
	}

	@Override
	public Long deleteCustomer(Long customerId)
	{
		if(customerRepository.existsById(customerId))
		{
			customerRepository.deleteById(customerId);
			return customerId;
		}
		return null;

	}

}
