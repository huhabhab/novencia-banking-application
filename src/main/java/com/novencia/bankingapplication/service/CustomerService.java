package com.novencia.bankingapplication.service;

import com.novencia.bankingapplication.entity.Customer;

import java.util.List;

public interface CustomerService
{
	List<Customer> getCustomers();

	Customer getCustomerById(Long id);

	Customer insert(Customer customer);

	void updateCustomer(Long id, Customer customer);

	Long deleteCustomer(Long customerId);

}
