package com.novencia.bankingapplication.service;

import com.novencia.bankingapplication.entity.Account;

import java.util.List;

public interface AccountService
{
	List<Account> getAccounts();

	Account getAccountById(Long id);

	Account insert(Account account);

	void updateAccount(Long id, Account account);

	Long deleteAccount(Long accountId);

}
