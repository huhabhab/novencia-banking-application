package com.novencia.bankingapplication.service;

import com.novencia.bankingapplication.entity.Account;
import com.novencia.bankingapplication.repository.AccountRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountServiceImpl implements AccountService
{
	AccountRepository accountRepository;

	public AccountServiceImpl(AccountRepository customerRepository)
	{
		this.accountRepository = customerRepository;
	}

	@Override
	public List<Account> getAccounts()
	{
		List<Account> accounts = new ArrayList<>();
		accountRepository.findAll().forEach(accounts::add);
		return accounts;
	}

	@Override
	public Account getAccountById(Long id)
	{
		return accountRepository.findById(id).get();
	}

	@Override
	public Account insert(Account account)
	{
		return accountRepository.save(account);
	}

	@Override
	public void updateAccount(Long id, Account account)
	{
		Account accountFromDb = accountRepository.findById(id).get();

		accountRepository.save(accountFromDb);
	}

	@Override
	public Long deleteAccount(Long accountId)
	{
		if(accountRepository.existsById(accountId))
		{
			accountRepository.deleteById(accountId);
			return accountId;
		}
		return null;

	}

}
