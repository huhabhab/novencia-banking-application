package com.novencia.bankingapplication.service;

import com.novencia.bankingapplication.entity.Transaction;
import com.novencia.bankingapplication.repository.TransactionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService
{
	TransactionRepository transactionRepository;

	public TransactionServiceImpl(TransactionRepository transactionRepository)
	{
		this.transactionRepository = transactionRepository;
	}

	@Override
	public List<Transaction> getTransactions()
	{
		List<Transaction> transactions = new ArrayList<>();
		transactionRepository.findAll().forEach(transactions::add);
		return transactions;
	}


	@Override
	public Transaction getTransactionById(Long id)
	{
		return transactionRepository.findById(id).get();
	}

	@Override
	public Transaction insert(Transaction transaction)
	{
		return transactionRepository.save(transaction);
	}

	@Override
	public void updateTransaction(Long id, Transaction transaction)
	{
		Transaction transactionFromDb = transactionRepository.findById(id).get();
		transactionRepository.save(transactionFromDb);
	}

	@Override
	public Long deleteTransaction(Long transactionId)
	{
		if(transactionRepository.existsById(transactionId))
		{
			transactionRepository.deleteById(transactionId);
			return transactionId;
		}
		return null;

	}

}
