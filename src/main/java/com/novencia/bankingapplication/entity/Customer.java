package com.novencia.bankingapplication.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(hidden = true)
	Long id;

	@Column(columnDefinition = "first_name")
	@Size(min=3,max=15)
	@NotNull String firstName;
	@Column(columnDefinition = "last_name")
	@Size(min=3,max=15)
	@NotNull String lastName;
	@Column(columnDefinition = "email_address")
	@Email
	@NotNull String emailAddress;
	@Pattern(regexp="(^$|[0-9]{10})")
	@Column(columnDefinition = "phone_number")
	@NotNull String phoneNumber;
	@CreationTimestamp
	@Column(updatable = false, columnDefinition = "created_date")
	@ApiModelProperty(hidden = true)
	LocalDateTime createdDate;
}


