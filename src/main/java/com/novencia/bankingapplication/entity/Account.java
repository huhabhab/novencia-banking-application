package com.novencia.bankingapplication.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Account
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(hidden = true)
	Long id;

	@Column(columnDefinition = "customer_id_fk")
	@NotNull int customerIdFk;
	@Column(columnDefinition = "currency_id_fk")
	@NotNull int currencyIdFk;
	@Column
	@NotNull double balance;
	@Column(updatable = false, columnDefinition = "created_date")
	@CreationTimestamp
	@ApiModelProperty(hidden = true)
	Timestamp createdDate;
	@Column(columnDefinition = "last_modified")
	@ApiModelProperty(hidden = true)
	@CreationTimestamp
	Timestamp lastModified;

}



