package com.novencia.bankingapplication.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Transaction
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(hidden = true)
	Long id;



	@Column(columnDefinition = "account_id_fk")
	@NotNull long accountIdFk;

	@Column
	@NotNull double amount;

	@Column
	@ApiModelProperty(hidden = true)
	@NotNull double balance;

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition = "transaction_type_fk")
	int transactionTypeFk;

	@ApiModelProperty(hidden = true)
	@Column(columnDefinition = "transaction_status_fk")
	int transactionStatusFk;

	@ApiModelProperty(hidden = true)
	@Column(updatable = false, columnDefinition = "created_date")
	@CreationTimestamp
	Timestamp createdDate;

}
