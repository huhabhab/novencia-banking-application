package com.novencia.bankingapplication.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
public class SwaggerConfig
{
	@Bean
	public Docket api()
	{
		return new Docket(DocumentationType.SWAGGER_2).select()
													  .paths(PathSelectors.ant("/api/**"))
													  .apis(RequestHandlerSelectors.basePackage("com.novencia.bankingapplication.controller"))
													  .build()
													  .apiInfo(apiInfo());
	}

	ApiInfo apiInfo()
	{
		return new ApiInfo("Novencia Banking REST APIs",
				"API for a personal bank account for the purpose of basic operations to control accounts, clients, and transactions", //description
				"Version 1.0", null, new Contact("Hussein Habhab", "https://www.linkedin.com/in/husseinhabhab/", "hussein.habhab@hotmail.com"), null,
				null, Collections.emptyList());
	}
}