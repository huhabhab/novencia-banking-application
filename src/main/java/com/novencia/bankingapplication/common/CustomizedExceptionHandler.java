/*
 * This class handles custom exceptions so that caught errors
 * would be more informative in the API response it self
 */
package com.novencia.bankingapplication.common;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@ControllerAdvice
public class CustomizedExceptionHandler extends ResponseEntityExceptionHandler
{
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status,
			WebRequest request)
	{
		List<String> errors = new ArrayList<String>();
		for(FieldError error : ex.getBindingResult().getFieldErrors())
		{
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}

		ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, "One or more invalid Request Parameters", errors);
		return handleExceptionInternal(ex, apiResponse, headers, apiResponse.getStatus(), request);

	}

	@ExceptionHandler({ NoSuchElementException.class })
	public ResponseEntity<Object> handleNoSuchElementException(NoSuchElementException ex, WebRequest request)
	{
		ApiResponse apiResponse = new ApiResponse(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), "Entity with provided ID not found");
		return new ResponseEntity<Object>(apiResponse, new HttpHeaders(), apiResponse.getStatus());
	}

	@ExceptionHandler({ EmptyResultDataAccessException.class })
	public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request)
	{
		ApiResponse apiResponse = new ApiResponse(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), "Entity with provided ID not found");
		return new ResponseEntity<Object>(apiResponse, new HttpHeaders(), apiResponse.getStatus());
	}

	@ExceptionHandler({ DataIntegrityViolationException.class })
	public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request)
	{
		ApiResponse apiResponse = new ApiResponse(HttpStatus.CONFLICT, ex.getCause().getCause().getLocalizedMessage(),
				"SQL Error - Constraint Violation");
		return new ResponseEntity<Object>(apiResponse, new HttpHeaders(), apiResponse.getStatus());
	}

	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleGenericException(Exception ex, WebRequest request)
	{
		ApiResponse apiResponse = new ApiResponse(HttpStatus.BAD_REQUEST, ex.getCause().getCause().getLocalizedMessage(), "Un handled Exception");
		return new ResponseEntity<Object>(apiResponse, new HttpHeaders(), apiResponse.getStatus());
	}
}
