/*
 * This class wraps the response of all API calls
 * in a unified object having the three params
 * status, message and the list of errors
 */
package com.novencia.bankingapplication.common;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Data
public class ApiResponse
{
	private HttpStatus status;
	private String message;
	private List<String> errors;

	public ApiResponse(HttpStatus status, String message, List<String> errors)
	{
		super();
		this.status = status;
		this.message = message;
		this.errors = errors;
	}

	public ApiResponse(HttpStatus status, String message, String error)
	{
		super();
		this.status = status;
		this.message = message;
		errors = Arrays.asList(error);
	}
}
