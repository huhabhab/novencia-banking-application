CREATE SCHEMA  IF NOT EXISTS  banking;

-----------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS banking.transaction CASCADE;
CREATE TABLE banking.transaction (
	id INT AUTO_INCREMENT,
	account_id_fk INT NULL,
	amount numeric NOT NULL,
	balance numeric NOT NULL,
	transaction_type_fk INT NULL,
	transaction_status_fk INT NULL,
	created_date timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id)
);

ALTER TABLE banking.transaction ADD FOREIGN KEY (account_id_fk) REFERENCES banking.account(id);
ALTER TABLE banking.transaction ADD FOREIGN KEY (transaction_status_fk) REFERENCES banking.transaction_status(id);
ALTER TABLE banking.transaction ADD FOREIGN KEY (transaction_type_fk) REFERENCES banking.transaction_type(id);

-----------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS banking.account CASCADE;
CREATE TABLE banking.account (
	id INT AUTO_INCREMENT,
	customer_id_fk INT NOT NULL,
	currency_id_fk INT NOT NULL,
	balance numeric NOT NULL,
	created_date timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	last_modified timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id)
);
ALTER TABLE banking.account ADD FOREIGN KEY (currency_id_fk) REFERENCES banking.currency(id);
ALTER TABLE banking.account ADD FOREIGN KEY (customer_id_fk) REFERENCES banking.customer(id);
CREATE UNIQUE INDEX constraint_unique_currency ON banking.account(customer_id_fk, currency_id_fk);
-----------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS banking.customer CASCADE;
CREATE TABLE banking.customer (
	id INT AUTO_INCREMENT,
	first_name varchar NOT NULL,
	last_name varchar NOT NULL,
	email_address varchar NOT NULL,
	phone_number varchar NOT NULL,
	created_date timestamp(0) not NULL,
	PRIMARY KEY (id)
);
ALTER TABLE banking.customer ALTER COLUMN created_date SET DEFAULT CURRENT_TIMESTAMP;

-----------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS banking.currency CASCADE;
CREATE TABLE banking.currency (
	id INT AUTO_INCREMENT,
	acronym varchar NOT NULL,
	description varchar NULL
);
-----------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS banking.exchange_rate CASCADE;
CREATE TABLE banking.exchange_rate (
	id INT AUTO_INCREMENT,
	currency_id_fk INT NOT NULL,
	rate numeric NOT NULL,
	last_modified timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	PRIMARY KEY (id)
);
-----------------------------------------------------------------------------------------------
DROP TABLE  IF EXISTS banking.transaction_type CASCADE ;
CREATE TABLE banking.transaction_type (
	id INT AUTO_INCREMENT,
	acronym varchar NOT NULL,
	description varchar NOT NULL,
	PRIMARY KEY (id)
);
-----------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS banking.transaction_status CASCADE;
CREATE TABLE banking.transaction_status (
	id INT AUTO_INCREMENT,
	status varchar NOT NULL,
	PRIMARY KEY (id)
);
-----------------------------------------------------------------------------------------------

