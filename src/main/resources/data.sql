INSERT INTO banking.currency (id,acronym,description) VALUES (1,'AUD','Australia Dollar');
INSERT INTO banking.currency (id,acronym,description) VALUES (2,'GBP','Great Britain Pound');
INSERT INTO banking.currency (id,acronym,description) VALUES (3,'EUR','Euro');
INSERT INTO banking.currency (id,acronym,description) VALUES (4,'JPY','Japan Yen');
INSERT INTO banking.currency (id,acronym,description) VALUES (5,'AED','United Arab Emirates Dirham');

INSERT INTO banking.transaction_type (id,acronym,description) VALUES (1,'DPST','Deposit');
INSERT INTO banking.transaction_type (id,acronym,description) VALUES (2,'WITH','Withdrawal');

INSERT INTO banking.transaction_status (id,status) VALUES (1,'SUCCESS');
INSERT INTO banking.transaction_status (id,status) VALUES (2,'FAILURE');

INSERT INTO banking.exchange_rate (id,currency_id_fk,rate,last_modified) VALUES (1,1,0.74,'2022-03-20 17:45:20');
INSERT INTO banking.exchange_rate (id,currency_id_fk,rate,last_modified) VALUES (2,2,1.32,'2022-03-20 17:45:20');
INSERT INTO banking.exchange_rate (id,currency_id_fk,rate,last_modified) VALUES (3,3,1.11,'2022-03-20 17:45:20');
INSERT INTO banking.exchange_rate (id,currency_id_fk,rate,last_modified) VALUES (4,4,0.008,'2022-03-20 17:45:20');
INSERT INTO banking.exchange_rate (id,currency_id_fk,rate,last_modified) VALUES (5,5,0.27,'2022-03-20 17:45:20');



