package Customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.novencia.bankingapplication.entity.Customer;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class CustomerHelper
{

	public static long[] addCustomer(boolean persist) throws IOException
	{
		long[] response = new long[2];

		Customer customer = new Customer();
		customer.setPhoneNumber("0070913671");
		customer.setEmailAddress("hussein.habhab@hotmail.com");
		customer.setFirstName("Hussein");
		customer.setLastName("Habhab");

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(customer);
		HttpUriRequest request = RequestBuilder.create("POST").setUri("http://localhost:8080/api/customer/add").
				setEntity(new StringEntity(json, ContentType.APPLICATION_JSON)).build();
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		response[0] = httpResponse.getStatusLine().getStatusCode();
		response[1] = Long.parseLong(httpResponse.getFirstHeader("CustomerId").getValue());
		if(!persist)
			CustomerHelper.deleteCustomer(response[1]);
		return response;
	}

	public static int addCustomer_Invalid() throws IOException
	{

		Customer customer = new Customer();
		customer.setPhoneNumber("0070913671");
		customer.setEmailAddress("invalid Email");
		customer.setFirstName("Hussein");
		customer.setLastName("Habhab");

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(customer);
		HttpUriRequest request = RequestBuilder.create("POST").setUri("http://localhost:8080/api/customer/add").
				setEntity(new StringEntity(json, ContentType.APPLICATION_JSON)).build();
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		return httpResponse.getStatusLine().getStatusCode();
	}

	/**
	 * Deletes a created customer provided his Id and returns the http response code
	 *
	 * @param customerId
	 * @return
	 * @throws IOException
	 */
	public static int deleteCustomer(long customerId) throws IOException
	{
		HttpUriRequest request = RequestBuilder.create("DELETE").setUri("http://localhost:8080/api/customer/delete/" + customerId).build();
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		return httpResponse.getStatusLine().getStatusCode();
	}

	/**
	 * @param customerId
	 * @return the customer object if found
	 * @throws IOException
	 */
	public static Customer getCustomerById(long customerId) throws IOException
	{

		HttpUriRequest request = new HttpGet("http://localhost:8080/api/customer/" + customerId);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		String text = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8")).lines()
																											   .collect(Collectors.joining("\n"));
		if(httpResponse.getStatusLine().getStatusCode() == 404)
		{
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		Customer c = mapper.readValue(text, Customer.class);
		return c;
	}
}
