package Customer;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class DeleteCustomer
{

	@Test
	public void deleteCustomer_Success() throws IOException
	{
		long[] addCustomerResponse = CustomerHelper.addCustomer(true);
		int response = CustomerHelper.deleteCustomer(addCustomerResponse[1]);
		assertEquals(200, response);
		CustomerHelper.deleteCustomer(addCustomerResponse[1]);
	}

	@Test
	public void deleteCustomer_NonExistingId() throws IOException
	{
		int response = CustomerHelper.deleteCustomer(1000);
		assertEquals(404, response);
	}

}


