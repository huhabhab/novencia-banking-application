package Customer;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.io.IOException;
import static org.junit.Assert.assertEquals;

@SpringBootTest
public class AddCustomer
{

	@Test
	public void addCustomer_Success() throws IOException
	{
		long[] addCustomerResponse = CustomerHelper.addCustomer(false);
		assertEquals(201, addCustomerResponse[0]);
	}

	@Test
	public void addCustomer_Invalid() throws IOException
	{
		int response = CustomerHelper.addCustomer_Invalid();
		assertEquals(400, response);
	}

}


