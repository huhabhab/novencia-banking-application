package Customer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.novencia.bankingapplication.entity.Customer;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class GetCustomer
{
	/**
	 * Try to get customers while no customers exist
	 *
	 * @throws IOException
	 */
	@Test
	public void getCustomer_Empty() throws IOException
	{
		HttpUriRequest request = new HttpGet("http://localhost:8080/api/customer/all");
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		assertEquals(200, httpResponse.getStatusLine().getStatusCode());
		String text = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8")).lines()
																											   .collect(Collectors.joining("\n"));
		assertEquals("[]", text);
	}

	/**
	 * Get customers provided that at least one customer exists
	 *
	 * @throws IOException
	 */
	@Test
	public void getCustomer_All() throws IOException
	{
		long[] addCustomerResponse = CustomerHelper.addCustomer(true);

		HttpUriRequest request = new HttpGet("http://localhost:8080/api/customer/all");
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		assertEquals(200, httpResponse.getStatusLine().getStatusCode());
		String text = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8")).lines()
																											   .collect(Collectors.joining("\n"));
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		Customer[] c = mapper.readValue(text, Customer[].class);
		assertEquals(c.length, 1);
		CustomerHelper.deleteCustomer(addCustomerResponse[1]);
	}

	@Test
	public void getCustomer_ById() throws IOException
	{
		long[] addCustomerResponse = CustomerHelper.addCustomer(true);
		Customer c = CustomerHelper.getCustomerById(addCustomerResponse[1]);
		assertEquals(new Long(addCustomerResponse[1]), c.getId());
		CustomerHelper.deleteCustomer(addCustomerResponse[1]);
	}

	@Test
	public void getCustomer_NonExistingId() throws IOException
	{
		Customer c = CustomerHelper.getCustomerById(1000);
		assertEquals(null, c);
	}

}


