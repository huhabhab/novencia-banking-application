package Account;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import Customer.CustomerHelper;
import com.novencia.bankingapplication.entity.Account;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class AccountHelper
{
	public static long[] addAccount(boolean persist) throws IOException
	{
		// create customer
		long[] customerResponse = CustomerHelper.addCustomer(true);
		long[] accountResponse = new long[3];

		Account account = new Account();
		account.setBalance(100);
		account.setCustomerIdFk((int) customerResponse[1]);
		account.setCurrencyIdFk(1);

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(account);
		HttpUriRequest request = RequestBuilder.create("POST").setUri("http://localhost:8080/api/account/add").
				setEntity(new StringEntity(json, ContentType.APPLICATION_JSON)).build();
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		accountResponse[0] = httpResponse.getStatusLine().getStatusCode();
		accountResponse[1] = Long.parseLong(httpResponse.getFirstHeader("AccountId").getValue());
		//store the corresponding customer id if needed
		accountResponse[2] = customerResponse[1];
		if(!persist)
		{
			AccountHelper.deleteAccount(accountResponse[1]);
			CustomerHelper.deleteCustomer(customerResponse[1]);
		}

		return accountResponse;
	}

	public static int addAccount_Invalid() throws IOException
	{

		long[] customerResponse = CustomerHelper.addCustomer(true);
		long[] accountResponse = new long[2];

		Account account = new Account();
		account.setBalance(100);
		account.setCustomerIdFk((int) customerResponse[1]);
		account.setCurrencyIdFk(1);

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(account);
		HttpUriRequest request = RequestBuilder.create("POST").setUri("http://localhost:8080/api/account/add").
				setEntity(new StringEntity(json, ContentType.APPLICATION_JSON)).build();
		// Add first Account
		HttpResponse httpResponse_1 = HttpClientBuilder.create().build().execute(request);
		// Second Account for same user with same currency
		HttpResponse httpResponse_2 = HttpClientBuilder.create().build().execute(request);
		AccountHelper.deleteAccount(Long.parseLong(httpResponse_1.getFirstHeader("AccountId").getValue()));
		CustomerHelper.deleteCustomer(customerResponse[1]);
		return httpResponse_2.getStatusLine().getStatusCode();
	}

	/**
	 * Deletes a created account provided his Id and returns the http response code
	 *
	 * @param accountId
	 * @return
	 * @throws IOException
	 */
	public static int deleteAccount(long accountId) throws IOException
	{
		HttpUriRequest request = RequestBuilder.create("DELETE").setUri("http://localhost:8080/api/account/delete/" + accountId).build();
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		return httpResponse.getStatusLine().getStatusCode();
	}

	public static Account getAccountById(long accountId) throws IOException
	{

		HttpUriRequest request = new HttpGet("http://localhost:8080/api/account/" + accountId);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		String text = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8")).lines()
																											   .collect(Collectors.joining("\n"));
		if(httpResponse.getStatusLine().getStatusCode() == 404)
		{
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		Account a = mapper.readValue(text, Account.class);
		return a;
	}
}
