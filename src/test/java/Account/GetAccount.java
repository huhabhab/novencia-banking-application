package Account;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import Customer.CustomerHelper;
import com.novencia.bankingapplication.entity.Account;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class GetAccount
{
	/**
	 * Try to get accounts while no accounts exist
	 *
	 * @throws IOException
	 */
	@Test
	public void getAccount_Empty() throws IOException
	{
		HttpUriRequest request = new HttpGet("http://localhost:8080/api/account/all");
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		assertEquals(200, httpResponse.getStatusLine().getStatusCode());
		String text = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8")).lines()
																											   .collect(Collectors.joining("\n"));
		assertEquals("[]", text);
	}

	/**
	 * Get accounts provided that at least one account exists
	 *
	 * @throws IOException
	 **/
	@Test
	public void getAccount_All() throws IOException
	{
		long[] addAccountResponse = AccountHelper.addAccount(true);

		HttpUriRequest request = new HttpGet("http://localhost:8080/api/account/all");
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		assertEquals(200, httpResponse.getStatusLine().getStatusCode());
		String text = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8")).lines()
																											   .collect(Collectors.joining("\n"));
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		Account[] a = mapper.readValue(text, Account[].class);
		assertEquals(a.length, 1);
		AccountHelper.deleteAccount(addAccountResponse[1]);
		CustomerHelper.deleteCustomer(addAccountResponse[2]);
	}

	@Test
	public void getAccount_ById() throws IOException
	{
		long[] addAccountResponse = AccountHelper.addAccount(true);
		Account a = AccountHelper.getAccountById(addAccountResponse[1]);
		assertEquals(new Long(addAccountResponse[1]), a.getId());
		AccountHelper.deleteAccount(addAccountResponse[1]);
		CustomerHelper.deleteCustomer(addAccountResponse[2]);
	}

	@Test
	public void getAccount_NonExistingId() throws IOException
	{
		Account a = AccountHelper.getAccountById(1000);
		assertEquals(null, a);
	}

}


