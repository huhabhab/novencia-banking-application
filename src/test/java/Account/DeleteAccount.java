package Account;

import Customer.CustomerHelper;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class DeleteAccount
{

	@Test
	public void deleteAccount_Success() throws IOException
	{
		long[] addAccountResponse = AccountHelper.addAccount(true);
		int response = AccountHelper.deleteAccount(addAccountResponse[1]);
		CustomerHelper.deleteCustomer(addAccountResponse[2]);
		assertEquals(200, response);
	}

	@Test
	public void deleteAccount_NonExistingId() throws IOException
	{
		int response = AccountHelper.deleteAccount(1000);
		assertEquals(404, response);
	}

}


