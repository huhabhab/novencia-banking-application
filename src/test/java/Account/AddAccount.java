package Account;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class AddAccount
{

	@Test
	public void addAccount_Success() throws IOException
	{
		long[] addAccountResponse = AccountHelper.addAccount(false);
		assertEquals(201, addAccountResponse[0]);
	}

	@Test
	public void addAccount_DuplicateCurrency() throws IOException
	{
		int addAccountResponse = AccountHelper.addAccount_Invalid();
		assertEquals(409, addAccountResponse);
		//delete the created customer

	}

}


