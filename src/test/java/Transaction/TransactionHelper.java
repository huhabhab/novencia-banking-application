package Transaction;

import Account.AccountHelper;
import Customer.CustomerHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.novencia.bankingapplication.entity.Transaction;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class TransactionHelper
{
	public static long[] addTransaction(boolean persist, int type, int amount) throws IOException
	{
		// create customer
		long[] accountResponse = AccountHelper.addAccount(true);
		long[] transactionResponse = new long[3];
		String action;

		if(type == 1)
		{
			action = "deposite";
		}
		else
		{
			action = "withdraw";
		}

		Transaction transaction = new Transaction();
		transaction.setTransactionTypeFk(type);
		transaction.setAmount(amount);
		transaction.setAccountIdFk(accountResponse[1]);

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		String json = ow.writeValueAsString(transaction);
		HttpUriRequest request = RequestBuilder.create("POST").setUri("http://localhost:8080/api/transactions/" + action).
				setEntity(new StringEntity(json, ContentType.APPLICATION_JSON)).build();
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		String text = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8")).lines()
																											   .collect(Collectors.joining("\n"));
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		Transaction t = mapper.readValue(text, Transaction.class);

		transactionResponse[0] = httpResponse.getStatusLine().getStatusCode();
		transactionResponse[1] = Long.parseLong(httpResponse.getFirstHeader("TransactionId").getValue());
		transactionResponse[2] = (long) t.getBalance();

		if(!persist)
		{
			deleteTransaction(transactionResponse[1]);
			AccountHelper.deleteAccount(accountResponse[1]);
			CustomerHelper.deleteCustomer(accountResponse[2]);
		}

		return transactionResponse;
	}

	/**
	 * Deletes a created transaction provided his Id and returns the http response code
	 *
	 * @param transactionId
	 * @return
	 * @throws IOException
	 */
	public static int deleteTransaction(long transactionId) throws IOException
	{
		HttpUriRequest request = RequestBuilder.create("DELETE").setUri("http://localhost:8080/api/transactions/delete/" + transactionId).build();
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		return httpResponse.getStatusLine().getStatusCode();
	}

}
