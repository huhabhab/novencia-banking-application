package Transaction;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class AddTransaction
{

	@Test
	public void Deposit_Success() throws IOException
	{
		long[] addTransactionResponse = TransactionHelper.addTransaction(false, 1, 50);
		assertEquals(201, addTransactionResponse[0]);
		assertEquals(150, addTransactionResponse[2]);
	}
	@Test
	public void Withdraw_Success() throws IOException
	{
		long[] addTransactionResponse = TransactionHelper.addTransaction(false, 2, 50);
		assertEquals(201, addTransactionResponse[0]);
		assertEquals(50, addTransactionResponse[2]);
	}

	@Test
	public void Withdraw_NoEnoughBalance() throws IOException
	{
		long[] addTransactionResponse = TransactionHelper.addTransaction(false, 2, 200);
		assertEquals(201, addTransactionResponse[0]);
		assertEquals(100, addTransactionResponse[2]);
	}

}


